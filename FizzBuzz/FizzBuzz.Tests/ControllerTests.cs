﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.Controllers;
using FizzBuzz.Models;
using System.Web.Mvc;

namespace FizzBuzz.Tests
{
    [TestClass]
    public class ControllerTests
    {
        [TestMethod]
        public void ControllerReturnsView()
        {
            var controller = new FizzBuzzController();
            var model = new FizzBuzzModel();
            var result = controller.Index(model) as ViewResult;
            Assert.AreEqual("Index", result.ViewName);
        }

    }
}
