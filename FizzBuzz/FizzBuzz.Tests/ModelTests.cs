﻿using System;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.Models;

namespace FizzBuzz.Tests
{
    [TestClass]
    public class ModelTests
    {
        [TestMethod]
        public void ModelUserInputCanBeSet()
        {
            var model = new FizzBuzzModel();
            model.userInput = 50;
            Assert.AreEqual(50, model.userInput);
        }

        [TestMethod]
        public void ModelReturnsArrayOfCorrectSize()
        {
            var model = new FizzBuzzModel();
            model.userInput = 25;

            // create values
            model.calcFizzBuzz();

            ArrayList result = model.fbValues;
            int arraySize = result.Count;
            Assert.AreEqual(25, arraySize);
            
        }

        [TestMethod]
        public void ModelReturnsCorrectValues()
        {
            var model = new FizzBuzzModel();
            model.userInput = 100;
            model.calcFizzBuzz();
            ArrayList result = model.fbValues;

            var value_0 = result[0];
            var value_1 = result[1];
            var value_2 = result[2];
            var value_4 = result[4];
            var value_14 = result[14];



            Assert.AreEqual(1, value_0);
            Assert.AreEqual(2, value_1);
            Assert.AreEqual("fizz", value_2);
            Assert.AreEqual("buzz", value_4);
            Assert.AreEqual("fizz buzz", value_14);

        }

    }
}
