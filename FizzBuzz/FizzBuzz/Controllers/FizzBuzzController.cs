﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.Models;

namespace FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        // First time page loads
        public ActionResult Index()
        {
            // Clears model validation so no errors appear when page first loaded
            ModelState.Remove("userInput");
            return View("Index");
        }

        // After form is submitted, run full validation
        [HttpPost]
        public ActionResult Index(FizzBuzzModel fb)
        {
            if(!ModelState.IsValid)
            {
                return View("Index");
            }

            fb.calcFizzBuzz();
            ViewBag.FB = fb.fbValues;

            return View("Index");
        }
    }
}