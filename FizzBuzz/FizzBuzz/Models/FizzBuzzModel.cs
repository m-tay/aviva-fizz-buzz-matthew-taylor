﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Models
{
    public class FizzBuzzModel
    {
        [Range(1, 1000, ErrorMessage = "Error: must be between 1 and 1000")]
        public int userInput { get; set; }
        public ArrayList fbValues { get; set; }


        public void calcFizzBuzz()
        {
            ArrayList fb = new ArrayList();

            for (int i = 1; i < (userInput + 1); i++)
            {
                if ((i % 3) == 0 && (i % 5) == 0)
                {
                    fb.Add("fizz buzz");
                }
                else if (i % 3 == 0)
                {
                    fb.Add("fizz");
                }
                else if (i % 5 == 0)
                {
                    fb.Add("buzz");
                }
                else
                {
                    fb.Add(i);
                }
                                
            }

            fbValues = fb;
        }


        
    }
}